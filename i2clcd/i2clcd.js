module.exports = function(RED) {
  'use strict';
  const LCD = require('lcdi2c');

  function LcdNode(config) {
    RED.nodes.createNode(this, config);

    this.address = config.address;
    this.format = config.format;

    let node = this;

    switch(node.format) {
    case 4:
      node.lcd = new LCD(1, node.address, 20, node.format);
      break;
    default:
      node.lcd = new LCD(1, node.address, 16, node.format);
    }
    node.lcd.clear();
    if (node.lcd.error) {
      lcdErrorHandler(node.lcd.error);
    } else {
      node.lcd.println('MOSi ScopePi', 1);
      this.status({
        fill: 'green',
        shape: 'dot',
        text: 'connected'
      });
    }

    function lcdErrorHandler(err) {
      node.error('Unable to print to LCD on bus 1 at address ' + this.address);
      this.status({
        fill: 'red',
        shape: 'ring',
        text: 'disconnected'
      });
      console.log(err);
    }

    function inputListener(msg) {
      if (RED.settings.verbose) { node.log('input: ' + msg.payload); }
      node.lcd.clear();
      if (!node.lcd.error) {
        for (let i=0, tot=msg.payload.length; i < tot; i++) {
          let lcdmsg = msg.payload[i].split(':');
          node.lcd.println(lcdmsg[1], lcdmsg[0]);
        }
      } else {
        lcdErrorHandler(node.lcd.error);
      }
    }

    node.on('input', inputListener);

    this.on('close', function() {
      //TODO: any cleanup?
    });
  }

  RED.nodes.registerType('i2c-lcd', LcdNode);
};